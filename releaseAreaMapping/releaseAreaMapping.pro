TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += . \
/home/matti/foam/foam-extend-4.0/src/ \
/home/matti/foam/foam-extend-4.0/src/finiteVolume \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/lnInclude \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/fvMesh

# Input
HEADERS += 

SOURCES += releaseAreaMapping.C 

OTHER_FILES += Make/files \
               Make/options \
               Make/releaseAreaMapping.includes
