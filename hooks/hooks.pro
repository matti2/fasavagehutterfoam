TEMPLATE = app

DEPENDPATH += .

INCLUDEPATH += . \
/home/matti/foam/foam-extend-4.0/src/ \
/home/matti/foam/foam-extend-4.0/src/finiteVolume \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/lnInclude \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/fvMesh

HEADERS += \
        cfdfunction.H \
        totalenergy/totalenergy.H \
        totalenergy/totalenergyFunctionObject.H \
        totalmomentum/totalmomentum.H \
        totalmomentum/totalmomentumFunctionObject.H \
        radar/radarFunctionObject.H \
        radar/radar.H \
        slabs/slabsFunctionObject.H \
        slabs/slabs.H \
        dynamicpressure/dynamicpressure.H \
        dynamicpressure/dynamicpressureFunctionObject.H
SOURCES += \
        cfdfunction.C \
        totalenergy/totalenergy.C \
        totalenergy/totalenergyFunctionObject.C \
        totalmomentum/totalmomentum.C \
        totalmomentum/totalmomentumFunctionObject.C \
        radar/radarFunctionObject.C \
        radar/radar.C \
        slabs/slabsFunctionObject.C \
        slabs/slabs.C \
        dynamicpressure/dynamicpressure.C \
        dynamicpressure/dynamicpressureFunctionObject.C

OTHER_FILES += \
        Make/files \
        Make/options

