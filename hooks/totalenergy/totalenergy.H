/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | faSavageHutterFOAM
    \\  /    A nd           | Copyright (C) 2017 Matthias Rauter
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::totalenergy

Description
    Calculates the kinetic, potential and inner energy.

SourceFiles
    totalenergy.C

Author
    Matthias Rauter matthias.rauter@uibk.ac.at

\*---------------------------------------------------------------------------*/

#ifndef totalenergy_H
#define totalenergy_H

#include "primitiveFieldsFwd.H"
#include "volFieldsFwd.H"
#include "HashSet.H"
#include "Tuple2.H"
#include "OFstream.H"
#include "Switch.H"
#include "pointFieldFwd.H"
#include "areaFields.H"


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{
    class objectRegistry;
    class mapPolyMesh;
/*---------------------------------------------------------------------------*\
                           Class enery Declaration
\*---------------------------------------------------------------------------*/

class totalenergy
{
public:


protected:

    // Private data

        const objectRegistry &obr_;

        //internal Fields
        areaScalarField loss_;

        //- The file for results
        autoPtr<OFstream> energyFilePtr_;

        //- Switch to send output to Info as well as to file
        Switch log_;

        //- The flow density
        dimensionedScalar rho_;


        //- Calculated initial Mass to calculate Gain/Loss
        dimensionedScalar initialMass_;

        //- Calculated minimal possible potential Energy as reference
        dimensionedScalar minimalEnergy_;

    // Private Member Functions

        //- Disallow default bitwise copy construct
        totalenergy(const totalenergy&);

        //- Disallow default bitwise assignment
        void operator=(const totalenergy&);

public:

    //- Runtime type information
        TypeName("totalenergy");


    // Constructors

        //- Construct for given objectRegistry and dictionary.
        //  Allow the possibility to load fields from files
        totalenergy
        (
            const word& name,
            const objectRegistry&,
            const dictionary&,
            const bool loadFromFiles = false
        );


    //- Destructor
        virtual ~totalenergy();


    // Member Functions

        //- Read the energy data
        virtual void read(const dictionary&);

        //- Execute
        virtual void execute();

        //- Execute at the final time-loop
        virtual void end();

        //- Write the energy
        virtual void write();

        //- Update for changes of mesh
        virtual void updateMesh(const mapPolyMesh&)
        {}

        //- Update for changes of mesh
        virtual void movePoints(const pointField&)
        {}

};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
