TEMPLATE = app
TARGET = 
DEPENDPATH += .
INCLUDEPATH += . \
/home/matti/foam/foam-extend-4.0/src/ \
/home/matti/foam/foam-extend-4.0/src/finiteVolume \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/lnInclude \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/fvMesh

# Input
HEADERS += \
    elevationModels/elevationModel/elevationModel.H \
    elevationModels/simpleSlope/SimpleSlope.H

SOURCES += \
    slopeMesh.C \
    elevationModels/elevationModel/newElevationModel.C \
    elevationModels/elevationModel/elevationModel.C \
    elevationModels/simpleSlope/SimpleSlope.C

OTHER_FILES += \
    Make/files \
    Make/options \
    Make/demMesh.includes
