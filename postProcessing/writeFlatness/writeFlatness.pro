TEMPLATE = app

DEPENDPATH += .

INCLUDEPATH += . \
/opt/foam/foam-extend-3.2/src/ \
/opt/foam/foam-extend-3.2/src/finiteVolume \
/opt/foam/foam-extend-3.2/src/finiteVolume/lnInclude \
/opt/foam/foam-extend-3.2/src/finiteVolume/fvMesh \
/opt/foam/foam-extend-3.2/src/ \
/opt/foam/foam-extend-3.2/src/finiteArea \
/opt/foam/foam-extend-3.2/src/finiteArea/lnInclude \
/opt/foam/foam-extend-3.2/src/finiteArea/fvMesh

HEADERS += 

SOURCES += writeFlatness.C

OTHER_FILES += Make/files \
               Make/options
