/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | faSavageHutterFOAM
    \\  /    A nd           | Copyright (C) 2017 Matthias Rauter
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    writeFlatness

Description
    Writes the flatness into a finite area field.

Author
    Matthias Rauter matthias.rauter@uibk.ac.at

\*---------------------------------------------------------------------------*/

#include "fvCFD.H"
#include "faCFD.H"
#include "HormannAgathos.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
#   include "setRootCase.H"
#   include "createTime.H"
#   include "createMesh.H"
#   include "createFaMesh.H"
#   include "readGravitationalAcceleration.H"

    areaVectorField n = aMesh.faceAreaNormals();


    areaScalarField flatness
    (
        IOobject
        (
            "flatness",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        aMesh,
        dimensionedScalar("zero", dimensionSet(0, 0, 0, 0, 0, 0, 0), 0)
    );

    volScalarField Flatness
    (
        IOobject
        (
            "Flatness",
            runTime.timeName(),
            mesh,
            IOobject::NO_READ,
            IOobject::AUTO_WRITE
        ),
        mesh,
        dimensionedScalar("zero", dimensionSet(0, 0, 0, 0, 0, 0, 0), 0)
    );


    const pointField& p = aMesh.points();
    const faceList& fcs = aMesh.faces();
    const pointField& fctrs = aMesh.areaCentres();

    // Areas are calculated as the sum of areas. (see
    // primitiveMeshFaceCentresAndAreas.C)
    scalarField magAreas(mag(aMesh.S()));

    label nWarped = 0;

    scalar minFlatness = GREAT;
    scalar sumFlatness = 0;
    label nSummed = 0;

    forAll(fcs, faceI)
    {
        const face& f = fcs[faceI];

        if (f.size() > 3 && magAreas[faceI] > VSMALL)
        {
            const point& fc = fctrs[faceI];

            // Calculate the sum of magnitude of areas and compare to magnitude
            // of sum of areas.

            scalar sumA = 0.0;

            forAll(f, fp)
            {
                const point& thisPoint = p[f[fp]];
                const point& nextPoint = p[f.nextLabel(fp)];

                // Triangle around fc.
                vector n = 0.5*((nextPoint - thisPoint)^(fc - thisPoint));
                sumA += mag(n);
            }

            scalar fn = magAreas[faceI] / (sumA+VSMALL);
            flatness[faceI] = fn;

            sumFlatness += fn;
            nSummed++;

            minFlatness = min(minFlatness, fn);

        }
    }

    if (nSummed > 0)
    {
       Info<< "    Face flatness (1 = flat, 0 = butterfly) : average = "
           << sumFlatness / nSummed << "  min = " << minFlatness << endl;
    }

    // Create volume-to surface mapping object
    volSurfaceMapping vsm(aMesh);
    vsm.mapToVolume(flatness, Flatness.boundaryField());
    Flatness.write();

    return(0);
}

// ************************************************************************* //
