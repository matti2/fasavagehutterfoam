/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | faSavageHutterFOAM
    \\  /    A nd           | Copyright (C) 2017 Matthias Rauter
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Application
    faMeshToAscii

Description
    Write finite area mesh into a csv file

Author
    Matthias Rauter matthias.rauter@uibk.ac.at

\*---------------------------------------------------------------------------*/



#include "fvCFD.H"
#include "faCFD.H"
#include "IOobjectList.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

int main(int argc, char *argv[])
{
#   include "setRootCase.H"
#   include "createTime.H"
#   include "createMesh.H"
#   include "createFaMesh.H"


    IOdictionary faMeshToAsciiDict
    (
         IOobject
         (
              "faMeshToAsciiDict",
              runTime.system(),
              mesh,
              IOobject::MUST_READ,
              IOobject::NO_WRITE
         )
    );


    vector offset;
    faMeshToAsciiDict.lookup("offset") >> offset;


    std::ostringstream buf;
    buf << "famesh.csv";

    std::ofstream csvfile;
    csvfile.open(buf.str().c_str());

    const pointField& p = aMesh.points();
    const faceList& fcs = aMesh.faces();

    forAll(fcs, faceI)
    {
        const face& f = fcs[faceI];

        forAll(f, fp)
        {
            const point& p1 = p[f[fp]];

            csvfile << p1[0]-offset[0] << ";" << p1[1]-offset[1] << ";" << p1[2]-offset[2] << ";";
        }
        csvfile << nl;
    }



    csvfile.close();



    return(0);

}

// ************************************************************************* //
