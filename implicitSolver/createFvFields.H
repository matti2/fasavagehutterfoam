volVectorField U
(
    IOobject
    (
        "U",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedVector("0", dimVelocity, vector::zero)
);

volScalarField H
(
    IOobject
    (
        "H",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedScalar("zero", dimensionSet(0, 1, 0, 0, 0, 0, 0), 0)
);

volScalarField Hentrain
(
    IOobject
    (
        "Hentrain",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedScalar("zero", dimensionSet(0, 1, 0, 0, 0, 0, 0), 0)
);

volScalarField Pb
(
    IOobject
    (
        "Pb",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedScalar("zero", dimensionSet(1, -1, -2, 0, 0, 0, 0), 0)
);

volVectorField Tau
(
    IOobject
    (
        "Tau",
        runTime.timeName(),
        mesh,
        IOobject::NO_READ,
        IOobject::AUTO_WRITE
    ),
    mesh,
    dimensionedVector("zero", dimensionSet(1, -1, -2, 0, 0, 0, 0), vector::zero)
);


#include "calcBasalstress.H"

// Create volume-to surface mapping object
volSurfaceMapping vsm(aMesh);
vsm.mapToVolume(Us, U.boundaryField());
vsm.mapToVolume(h, H.boundaryField());
vsm.mapToVolume(pb, Pb.boundaryField());
U.write();
H.write();
Pb.write();
