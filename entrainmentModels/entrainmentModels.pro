TEMPLATE = app

DEPENDPATH += .
INCLUDEPATH += . \
/home/matti/foam/foam-extend-4.0/src/ \
/home/matti/foam/foam-extend-4.0/src/finiteVolume \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/lnInclude \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/fvMesh

HEADERS +=  \
        entrainmentModel/entrainmentModel.H \
        entrainmentModel/newEntrainmentModel.H \
        entrainmentOff/entrainmentOff.H \
        Front/Front.H \
        Erosionenergy/Erosionenergy.H \
        IsslerFC/IsslerFC.H \
        Medina/Medina.H \
        depositionModel/depositionModel.H \
        depositionModel/newDepositionModel.H \
        depositionOff/depositionOff.H \
        Stopingprofile/Stopingprofile.H 

SOURCES += \
        entrainmentModel/newEntrainmentModel.C \
        entrainmentModel/entrainmentModel.C \
        entrainmentOff/entrainmentOff.C \
        Front/Front.C \
        Erosionenergy/Erosionenergy.C \
        IsslerFC/IsslerFC.C \
        Medina/Medina.C \
        depositionModel/newDepositionModel.C \
        depositionModel/depositionModel.C \
        depositionOff/depositionOff.C \
        Stopingprofile/Stopingprofile.C 


OTHER_FILES += \
    Make/files \
    Make/options
