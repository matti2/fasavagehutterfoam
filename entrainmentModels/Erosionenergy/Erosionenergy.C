/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     | faSavageHutterFOAM
    \\  /    A nd           | Copyright (C) 2017 Matthias Rauter
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Author
    Matthias Rauter matthias.rauter@uibk.ac.at

\*---------------------------------------------------------------------------*/

#include "faCFD.H"
#include "Erosionenergy.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace entrainmentModels
{
    defineTypeNameAndDebug(Erosionenergy, 0);
    addToRunTimeSelectionTable(entrainmentModel, Erosionenergy, dictionary);
}
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::entrainmentModels::Erosionenergy::Erosionenergy
(
    const word& name,
    const dictionary& entrainmentProperties,
    const areaVectorField &Us,
    const areaScalarField &h,
    const areaScalarField &hentrain,
    const areaScalarField &pb,
    const areaVectorField &tau
)
:
    entrainmentModel(name, entrainmentProperties, Us, h, hentrain, pb, tau),
    eb_(entrainmentProperties.subDict("ErosionenergyCoeffs").lookup("eb")),
    gs_(Us_.db().lookupObject<areaVectorField>("gs")),
    gn_(Us_.db().lookupObject<areaScalarField>("gn"))
{
    Info << "    " << eb_ << endl << endl;

}


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //


const Foam::areaScalarField &Foam::entrainmentModels::Erosionenergy::Sm() const
{
    Sm_ = (tau_&Us_)/eb_/rho_;
    Sm_ = min(Sm_, hentrain_/Us_.db().time().deltaT());

    return Sm_;
}

bool Foam::entrainmentModels::Erosionenergy::read
(
    const dictionary& entrainmentProperties
)
{
    entrainmentModel::read(entrainmentProperties);

    entrainmentProperties_.lookup("eb") >> eb_;

    return true;
}


// ************************************************************************* //
