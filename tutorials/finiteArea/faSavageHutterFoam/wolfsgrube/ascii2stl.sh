#!/bin/sh

if ../../../../scripts/txt2mesh.py \
-exactcopy \
-i rawdata/dem.asc \
-o dem.stl;
then
    echo "   OK"
else 
	echo "   FAILED"
	exit 1
fi
