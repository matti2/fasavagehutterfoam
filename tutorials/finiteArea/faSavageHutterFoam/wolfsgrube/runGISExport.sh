#!/bin/sh


python ../../../../scripts/foam2shape.py \
-offsetx "2000" \
-offsety "-221000" \
-fields "0/h 5/h 10/h 15/h 20/h 25/h 30/h 35/h 40/h 45/h 50/h 55/h 60/h 65/h 70/h 150/h" -o hpolys.shp

python ../../../../scripts/foam2shape.py \
-offsetx "2000" \
-offsety "-221000" \
 -fields "0/hentrain 5/hentrain 10/hentrain 15/hentrain 20/hentrain 25/hentrain 30/hentrain 35/hentrain 40/hentrain 45/hentrain 50/hentrain 55/hentrain 60/hentrain 65/hentrain 70/hentrain 150/hentrain" -o hentrainpolys.shp

python ../../../../scripts/foam2shape.py \
-offsetx "2000" \
-offsety "-221000" \
 -fields "0/Us 5/Us 10/Us 15/Us 20/Us 25/Us 30/Us 35/Us 40/Us 45/Us 50/Us 55/Us 60/Us 65/Us 70/Us 150/Us" -o Uspolys.shp




python ../../../../scripts/foam2shape.py \
-offsetx "2000" \
-offsety "-221000" \
 -fields "0/h 5/h 10/h 15/h 20/h 25/h 30/h 35/h 40/h 45/h 50/h 55/h 60/h 65/h 70/h 150/h" -asPoints -o hfields.shp

python ../../../../scripts/foam2shape.py \
-offsetx "2000" \
-offsety "-221000" \
 -fields "0/hentrain 5/hentrain 10/hentrain 15/hentrain 20/hentrain 25/hentrain 30/hentrain 35/hentrain 40/hentrain 45/hentrain 50/hentrain 55/hentrain 60/hentrain 65/hentrain 70/hentrain 150/hentrain" -asPoints -o hentrainfields.shp

python ../../../../scripts/foam2shape.py \
-offsetx "2000" \
-offsety "-221000" \
 -fields "0/Us 5/Us 10/Us 15/Us 20/Us 25/Us 30/Us 35/Us 40/Us 45/Us 50/Us 55/Us 60/Us 65/Us 70/Us 150/Us" -asPoints -o Usfields.shp
