#!/bin/sh

echo "clearing results..."
rm -rf 0.* [1-9]* demMesh.log makeFaMesh.log releaseAreaMapping.log faSavageHutterFoam.log 
rm -rf processor*
rm -rf 0
rm -rf constant/polyMesh
cp -r org0 0

mkdir constant/polyMesh
echo -n "creating FVMesh..."
if ../../../../scripts/txt2mesh.py \
-xres 200 -yres 400 \
-p1 "-3609., 219911" -p2 "-4800., 221820."  -p3 "-4100., 222350." -p4 "-2880., 220200." \
-fillup \
-offsetx "2000" \
-offsety "-221000" \
-butterflyangle 5 \
-i rawdata/dem.asc \
-mesh -o constant/polyMesh;
then
    echo "   OK"
else 
	echo "   FAILED"
	exit 1
fi
