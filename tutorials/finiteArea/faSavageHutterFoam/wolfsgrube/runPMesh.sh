#!/bin/sh

DH=40
NR=2


echo "clearing results..."
rm -rf 0.* [1-9]* demMesh.log makeFaMesh.log releaseAreaMapping.log faSavageHutterFoam.log 
rm -rf processor*
rm -rf 0
rm -rf constant/polyMesh
cp -r org0 0


echo -n "creating FVMesh..."
if ../../../../scripts/txt2mesh.py \
-xres 200 -yres 400 \
-p1 "-3609., 219911" -p2 "-4800., 221820."  -p3 "-4100., 222350." -p4 "-2880., 220200." \
-fillup \
-i rawdata/dem.asc \
-walls \
-offsetx "2000" \
-offsety "-221000" \
-o surface.stl;
then
    echo "   OK"
else 
	echo "   FAILED"
	exit 1
fi


echo "writing meshDict for H=$DH, Refinements=$NR"
sed "s/DH/$DH/g" system/meshDict.org > system/meshDict.1
sed "s/NR/$NR/g" system/meshDict.1 > system/meshDict
rm system/meshDict.1

echo -n "running pMesh"
if pMesh > log.pMesh; then
    echo "   OK"
else 
    echo "   FAILED"
    exit 1
fi

echo "checking Mesh"
checkMesh > log.checkMesh
