TEMPLATE = app

DEPENDPATH += .
INCLUDEPATH += . \
/home/matti/foam/foam-extend-4.0/src/ \
/home/matti/foam/foam-extend-4.0/src/finiteVolume \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/lnInclude \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/fvMesh

HEADERS +=  \
        frictionModel/frictionModel.H \
        Voellmy/Voellmy.H \
        kt/kt.H \
        PoliquenForterre/PoliquenForterre.H \
        DarcyWeisbach/DarcyWeisbach.H \
        muI/muI.H \
        ManningStrickler/ManningStrickler.H

SOURCES += \
        frictionModel/newFrictionModel.C \
        frictionModel/frictionModel.C \
        Voellmy/Voellmy.C \
        kt/kt.C \
        PoliquenForterre/PoliquenForterre.C \
        muI/muI.C \
        DarcyWeisbach/DarcyWeisbach.C \
        ManningStrickler/ManningStrickler.C

OTHER_FILES += \
    Make/files \
    Make/options \
