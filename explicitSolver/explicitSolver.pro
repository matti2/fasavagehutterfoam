TEMPLATE = app

DEPENDPATH += .
INCLUDEPATH += . \
/home/matti/foam/foam-extend-4.0/src/ \
/home/matti/foam/foam-extend-4.0/src/finiteVolume \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/lnInclude \
/home/matti/foam/foam-extend-4.0/src/finiteVolume/fvMesh

HEADERS += \
    readSolutionControls.H \
    readTransportProperties.H \
    createFaFields.H \
    surfaceCourantNo.H \
    createFvFields.H \
    calcBasalstress.H \
    readGravitationalAcceleration.H \
    calcFluxes.h

SOURCES += \
    faSavageHutterFoamExp.C \

OTHER_FILES += \
    Make/files \
    Make/options \
