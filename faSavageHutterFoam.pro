TEMPLATE = subdirs

SUBDIRS = \
    explicitSolver \
    implicitSolver \
    releaseAreaMapping \
    slopeMesh \
    hooks \
    frictionModels \
    entrainmentModels \
    postProcessing/depositeAll \
    postProcessing/fafieldsToAscii \
    postProcessing/faMeshToAscii \
    postProcessing/writeFlatness
